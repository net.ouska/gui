package net.ouska.gui;

import javax.swing.SwingConstants;

public enum Alignment {
	DEFAULT(-1, -1),
	LEFT_TOP(SwingConstants.LEFT, SwingConstants.TOP), 
	LEFT_CENTER(SwingConstants.LEFT, SwingConstants.CENTER),
	LEFT_BOTTOM(SwingConstants.LEFT, SwingConstants.BOTTOM),   
	CENTER_TOP(SwingConstants.CENTER, SwingConstants.TOP),
	CENTER(SwingConstants.CENTER, SwingConstants.CENTER),
	CENTER_BOTTOM(SwingConstants.CENTER, SwingConstants.BOTTOM),
	RIGHT_TOP(SwingConstants.RIGHT, SwingConstants.TOP),
	RIGHT_CENTER(SwingConstants.RIGHT, SwingConstants.CENTER),
	RIGHT_BOTTOM(SwingConstants.RIGHT, SwingConstants.BOTTOM);
	
	private final int horizontal;
	private final int vertical;
	
	private Alignment(int horizontal, int vertical) {
		this.horizontal = horizontal;
		this.vertical = vertical;
	}

	public int getHorizontal() {
		return horizontal;
	}

	public int getVertical() {
		return vertical;
	}
}
