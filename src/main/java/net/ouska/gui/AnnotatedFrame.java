package net.ouska.gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 * The predecessor of the JFrames defining the widgets and GUI using annotations.
 * The typical usage is as follows:
 * <pre> 
    <code>@Window(title="Nadpis", rows=3, cols=5)</code>
    public class TestWindow extends AnnotatedFrame {
    <code>@Widget(x=1,y=1,text="Name:")</code>
    JLabel lblName;
	<code>@Widget(x=2,y=1,cols=4)</code>
	JTextField txtName;
	<code>@Widget(x=5,y=2,text="OK",name="btn1")</code>
	JButton button;
	
	@OnClick("btn1")
	private void onAction() {
  		showMessageDialog("Button clicked");
	}
	
	public static void main(String[] args) {
		open(TestWindow.class);
 	}
 }
 </pre>
 */
public class AnnotatedFrame extends JFrame implements ActionListener {
 
	private static final long serialVersionUID = 1L;
	
	protected int colWidth;
    protected int rowHeight;
    protected int colPadding;
    protected int rowPadding;
    protected Map<String, ButtonGroup> buttonGroups = new HashMap<String, ButtonGroup>();
	
	protected void initInternal() {
		this.pack();
		this.setLayout(null);
		
		Field[] fields = this.getClass().getDeclaredFields();
		if (fields != null) {
			for (Field field : fields) {
				interceptField(field);
			}
		}
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				onClosed();
			}
			@Override
			public void windowOpened(WindowEvent e) {
				onOpened();
			}
		});
		init();
	}

	protected void init() {

	}

	protected void onClosed() {

	}

	protected void onOpened() {

	}

	private void interceptField(Field field) {
		try {
			field.setAccessible(true);
			Widget w = field.getAnnotation(Widget.class);
			if (w != null) {
				Object object = field.get(this);
				
				if (object == null) {
					object = field.getType().newInstance();
					field.set(this, object);
				}
				
				if (object instanceof JComponent) {
					JComponent component = (JComponent) field.get(this);
					
					if (component instanceof JTextArea) {
						JTextArea textArea = (JTextArea) component;
				    	Font font = textArea.getFont();
				    	textArea.setFont(new Font(font.getName(), Font.PLAIN, 12));
				    	JScrollPane scrollPane = new JScrollPane(textArea);
				    	scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
				    	scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
				    	
				    	addToPanel(scrollPane, w);
					} else {
						addToPanel(component, w);
					}
					
					if (w.text() != null && w.text().length() > 0) {
						if (object instanceof JTextComponent) {
							((JTextComponent) component).setText(w.text());
						} else if (object instanceof JLabel) {
							((JLabel) component).setText(w.text());
							Alignment al = w.alignment() == Alignment.DEFAULT ? Alignment.LEFT_CENTER : w.alignment();
							((JLabel) component).setHorizontalAlignment(al.getHorizontal());
							((JLabel) component).setVerticalAlignment(al.getVertical());
						} else if (object instanceof AbstractButton) {
							((AbstractButton) component).setText(w.text());
							Alignment al = w.alignment();
							if (w.alignment() == Alignment.DEFAULT && component instanceof JToggleButton) {
								al = Alignment.LEFT_CENTER;
							} else {
								al = Alignment.CENTER;
							}
							((AbstractButton) component).setHorizontalAlignment(al.getHorizontal());
							((AbstractButton) component).setVerticalAlignment(al.getVertical());
						}
					}
					
					if (component instanceof AbstractButton) {
						AbstractButton button = (AbstractButton) component;
						button.addActionListener(this);
						if (w.defaultEnterButton() && button instanceof JButton) {
							SwingUtilities.getRootPane(button).setDefaultButton((JButton)button);	
						}
					}
				} else {
					System.err.println("Annotation widget can be used with JComponent. It is used with " + field.getName());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void addToPanel(JComponent component, Widget w) {
		if (component instanceof JRadioButton) {
			ButtonGroup bg = buttonGroups.get(w.radioGroup());
			if (bg == null) {
				bg = new ButtonGroup();
				buttonGroups.put(w.radioGroup(), bg);
			}
			bg.add((JRadioButton)component);
		}
		
		if (w.position() == Position.ABSOLUTE) {
			component.setBounds(w.x(), w.y(), w.width(), w.height());	
		} else if (w.position() == Position.BOX) {
			int x = colWidth  * (w.x()-1) + 2*w.x()*colPadding - colPadding;
			int y = rowHeight * (w.y()-1) + 2*w.y()*rowPadding - rowPadding;
			int width = w.cols() * (colWidth + 2*colPadding) - 2*colPadding;
			int height = w.rows() * (rowHeight + 2*rowPadding) - 2*rowPadding;
			component.setBounds(x, y, width, height);
		} else {
			System.err.println("Unknown positioning " + w.position());
		}
		getContentPane().add(component);
	}
	
	/**
	 * The method that is called when a button is clicked. Can be overriden.
	 * @param button clicked button.
	 */
	public void onClick(AbstractButton button) {
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof AbstractButton) {
			AbstractButton button = (AbstractButton) e.getSource();
			onClick(button);
			
			try {
				Method[] methods = this.getClass().getDeclaredMethods();
				if (methods != null) {
					for (Method method : methods) {
						if (method.isAnnotationPresent(OnClick.class)) {
							OnClick onClick = method.getAnnotation(OnClick.class);
							String name = onClick.value();
	
							Field[] fields = this.getClass().getDeclaredFields();
							if (fields != null) {
								for (Field field : fields) {
									field.setAccessible(true);
									if (field.get(this) == button 
										&& field.isAnnotationPresent(Widget.class)) {
									
										String buttonName = field.getAnnotation(Widget.class).name();
										if (buttonName.isEmpty()) {
											buttonName = field.getName();
										} 
										
										if (name.isEmpty() || name.equals(buttonName)) {
											Parameter[] params = method.getParameters();
											Object[] paramValues = new Object[params.length];
											if (params != null) {
												for (int i = 0; i < params.length; i++) {
													java.lang.reflect.Type type = params[i].getParameterizedType();
													if (type.getTypeName().equals(ActionEvent.class.getName())) {
														paramValues[i] = e;
													} else if (button instanceof JButton && type.getTypeName().equals(JButton.class.getName())) {
														paramValues[i] = (JButton) button;
													} else if (type.getTypeName().equals(AbstractButton.class.getName())) {														
														paramValues[i] = button;
													}
												}
											}
											method.setAccessible(true);
											method.invoke(this, paramValues);
										}
									}
								}
							}
						}
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Shows message dialog.
	 * @param title
	 * @param message
	 */
	protected void showMessageDialog(String title, String message) {
		GUI.showMessageDialog(this, title, message);	
	}

	/**
	 * Shows message dialog.
	 * @param message
	 */
	protected void showMessageDialog(String message) {
		GUI.showMessageDialog(this, "Info", message);
	}

	/**
	 * Shows warning dialog.
	 * @param title
	 * @param message
	 */
	protected void showWarningDialog(String title, String message) {
		GUI.showWarningDialog(this, title, message);	
	}

	/**
	 * Shows warning dialog.
	 * @param message
	 */
	protected void showWarningDialog(String message) {
		GUI.showWarningDialog(this, "Warning", message);
	}

	/**
	 * Shows error dialog.
	 * @param title
	 * @param message
	 */
	protected void showErrorDialog(String title, String message) {
		GUI.showErrorDialog(this, title, message);	
	}

	/**
	 * Shows error dialog.
	 * @param message
	 */
	protected void showErrorDialog(String message) {
		GUI.showErrorDialog(this, "Error", message);
	}

	/**
	 * Shows open file dialog.
	 * @return The selected file or null if the user has cancelled the selection.
	 */
	protected File showOpenFileDialog() {
		return GUI.showOpenFileDialog(this);
	}
 
	/**
	 * Shows open save dialog.
	 * @return The selected file or null if the user has cancelled the selection.
	 */
	protected File showSaveFileDialog() {
		return GUI.showSaveFileDialog(this);
	}

	/**
	 * Shows the YesNo dialog.
	 * @param title
	 * @param message
	 * @return true if the user clicked the Yes button. Otherwise false.
	 */
	protected boolean showYesNoDialog(String title, String message) {
		return GUI.showYesNoDialog(this, title, message);
	}

	/**
	 * Shows the YesNo dialog.
	 * @param message
	 * @return true if the user clicked the Yes button. Otherwise false.
	 */
	protected boolean showYesNoDialog(String message) {
		return GUI.showYesNoDialog(this, message);
	}

	/**
	 * Shows the YesNoCancel dialog.
	 * @param title
	 * @param message
	 * @return true if the user clicked the Yes button. false if the user clicked the No button. Null otherwise.
	 */
	protected Boolean showYesNoCancelDialog(String title, String message) {
		return GUI.showYesNoCancelDialog(this, title, message);
	}

	/**
	 * Shows the YesNoCancel dialog.
	 * @param message
	 * @return true if the user clicked the Yes button. false if the user clicked the No button. Null otherwise.
	 */
	protected Boolean showYesNoCancelDialog(String message) {
		return GUI.showYesNoCancelDialog(this, message);
	}

	/**
	 * Shows the OKCancel dialog.
	 * @param title
	 * @param message
	 * @return true if the user clicked the Ok button. Otherwise false.
	 */
	protected boolean showOkCancelDialog(String title, String message) {
		return GUI.showOkCancelDialog(this, title, message);
	}

	/**
	 * Shows the OKCancel dialog.
	 * @param message
	 * @return true if the user clicked the Ok button. Otherwise false.
	 */
	protected boolean showOkCancelDialog(String message) {
		return GUI.showOkCancelDialog(this, message);
	}

	/**
	 * Opens the initial frame/window.
	 * @param frameClass class of the frame - f.e. open(MyWindow.class);
	 */
	public static void open(final Class<? extends AnnotatedFrame> frameClass) {
		GUI.open(frameClass);	
	}

	/**
	 * Shows the subsequential frame/window.
	 * @param frameClass class of the frame - f.e. showFrame(MyWindow.class);
	 * @return the created frame instance.
	 */
	public <AF extends AnnotatedFrame> AF showFrame(final Class<AF> frameClass) {
		return GUI.showAnnotatedFrameInUIThread(this, frameClass);	
	}
}
