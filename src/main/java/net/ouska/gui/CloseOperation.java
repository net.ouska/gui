package net.ouska.gui;

import javax.swing.JFrame;

public enum CloseOperation {
	
	EXIT_ON_CLOSE(JFrame.EXIT_ON_CLOSE),
	DISPOSE_ON_CLOSE(JFrame.DISPOSE_ON_CLOSE),
	DO_NOTHING_ON_CLOSE(JFrame.DO_NOTHING_ON_CLOSE),
	HIDE_ON_CLOSE(JFrame.HIDE_ON_CLOSE);
	
	private final int code;
	
	private CloseOperation(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
