package net.ouska.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class GUI {
	
	public static void open(final Class<? extends AnnotatedFrame> frameClass) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			SwingUtilities.invokeLater(new Runnable() {
		        @Override
		        public void run() {
		        	try {
		        		showAnnotatedFrameInUIThread(null, frameClass);
		        	} catch (Exception e) {
						e.printStackTrace();
					}
		        }
		    });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static <AF extends AnnotatedFrame> AF showAnnotatedFrameInUIThread(Component parent, final Class<AF> frameClass) {
		try {
			AnnotatedFrame af = frameClass.newInstance();
			Window w = af.getClass().getAnnotation(Window.class);
	    	int width = 0;
	    	int height = 0;
			if (w == null) {
				System.err.println("Frame class is missing @Window annotation.");
				af.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    	af.setTitle("Dialog");
		    	af.setLocationRelativeTo(null);	
		    	af.colPadding = 5;
	    		af.rowPadding = 5;
	    		af.colWidth = 50;
	    		af.rowHeight = 25;
	    		width = 5 * (af.colWidth + af.colPadding);
	    		height = 10 * (af.rowHeight + af.rowPadding);
			} else {
				af.setDefaultCloseOperation(w.defaultCloseOperation().getCode());
		    	af.setTitle(w.title());
		    	if (w.x() == -1 && w.y() == -1) {
		    		af.setLocationRelativeTo(parent);	
		    	} else {
		    		af.setLocation(w.x(), w.y());
		    	}
		    	
		    	af.colPadding = w.colPadding();
	    		af.rowPadding = w.rowPadding();
	    		af.colWidth = w.colWidth();
	    		af.rowHeight = w.rowHeight();
	    		if (w.width() == -1 && w.height() == -1) {
		    		width = w.cols() * (w.colWidth() + 2*w.colPadding());
		    		height = w.rows() * (w.rowHeight() + 2*w.rowPadding());
		    	} else {
		    		width = w.width();
		    		height = w.height();
		    	}				
			}

	    	af.pack();
	    	Insets ins =af.getInsets();
	    	height = height + ins.top + ins.bottom;
	    	width = width + ins.left + ins.right;
	    	af.setPreferredSize(new Dimension(width, height));
	    	af.pack();
	    	
			if (w == null) {
		    	af.setLocationRelativeTo(null);	
			} else {
		    	af.setTitle(w.title());
		    	if (w.x() == -1 && w.y() == -1) {
		    		af.setLocationRelativeTo(parent);	
		    	} else {
		    		af.setLocation(w.x(), w.y());
		    	}
			}	    	
	    	
	    	af.initInternal();			        	
	    	af.setVisible(true);
	    	
	    	return (AF) af;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Cannot create a new instance of " + frameClass.getName());
		}
	}
	
	public static void showMessageDialog(Component component, String title, String message) {
		JOptionPane.showMessageDialog(component, message, title, JOptionPane.PLAIN_MESSAGE);	
	}

	public static void showMessageDialog(Component component, String message) {
		showMessageDialog(component, "Info", message);
	}

	public static void showWarningDialog(Component component, String title, String message) {
		JOptionPane.showMessageDialog(component, message, title, JOptionPane.WARNING_MESSAGE);	
	}

	public static void showWarningDialog(Component component, String message) {
		showWarningDialog(component, "Warning", message);	
	}

	public static void showErrorDialog(Component component, String title, String message) {
		JOptionPane.showMessageDialog(component, message, title, JOptionPane.ERROR_MESSAGE);	
	}

	public static void showErrorDialog(Component component, String message) {
		showErrorDialog(component, "Error", message);	
	}

	public static boolean showYesNoDialog(Component component, String title, String message) {
		int dialogResult = JOptionPane.showConfirmDialog(
			component, message, title, 
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE);
		return dialogResult == JOptionPane.YES_OPTION;
	}

	public static boolean showYesNoDialog(Component component, String message) {
		return showYesNoDialog(component, "Question", message);	
	}
	
	public static Boolean showYesNoCancelDialog(Component component, String title, String message) {
		int dialogResult = JOptionPane.showConfirmDialog(
			component, message, title, 
			JOptionPane.YES_NO_CANCEL_OPTION,
			JOptionPane.QUESTION_MESSAGE);
		if (dialogResult == JOptionPane.YES_OPTION){
		  return true;
		} else if (dialogResult == JOptionPane.NO_OPTION) {
			return false;
		} else {
			return null;
		}
	}
	
	public static Boolean showYesNoCancelDialog(Component component, String message) {
		return showYesNoCancelDialog(component, "Question", message);	
	}
	
	public static boolean showOkCancelDialog(Component component, String title, String message) {
		int dialogResult = JOptionPane.showConfirmDialog(
			component, message, title, 
			JOptionPane.OK_CANCEL_OPTION,
			JOptionPane.QUESTION_MESSAGE);
		return dialogResult == JOptionPane.OK_OPTION;
	}

	public static boolean showOkCancelDialog(Component component, String message) {
		return showOkCancelDialog(component, "Question", message);	
	}

	public static File showOpenFileDialog(Component component) {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(component);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile();
        } else {
            return null;
        }
	}	

	public static File showSaveFileDialog(Component component) {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showSaveDialog(component);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile();
        } else {
            return null;
        }
	}	
}
