package net.ouska.gui;

public enum Position {
	/** The position of the widget is specified in absolute pixels. */
	ABSOLUTE,
	/** The position of the widget is specified by boxes. */
	BOX
}
