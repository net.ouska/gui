package net.ouska.gui;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME) 
public @interface Widget {
	/** 
	 * Widget x position in pixels if the position parameter is set to Position.ABSOLUTE.
	 * Widget x position in pixels if the position parameter is set to Position.BOX.
     */
	int x();
	/** 
	 * Widget y position in pixels if the position parameter is set to Position.ABSOLUTE.
	 * Widget y position in pixels if the position parameter is set to Position.BOX.
     */
	int y();
	/**
	 * The way how to locate the widget x, y position - absolute or boxes. 
	 */
    Position position() default Position.BOX;
    /**
     * The count of rows occupied by this widget in case of BOX position.
     */
    int rows() default 1;
    /**
     * The count of columns occupied by this widget in case of BOX position. 
     */
    int cols() default 1;
    /**
     * The widget width in case of ABSOLUTE position. 
     */
    int width() default 100;
    /**
     * The widget height.
     */
    int height() default 25;
    /**
     * Name of the widget used with OnClick annotation.
     */
    String name() default "";
    /**
     * Text of the widget.
     */
    String text() default "";
    /**
     * Text alignment - see Alignment.
     */
    Alignment alignment() default Alignment.DEFAULT;
    /**
     * Group name for radiobuttons
     */
    String radioGroup() default "";
    
    /**
     * Whether the click on this button should be performed when user press Enter key.
     */
    boolean defaultEnterButton() default false;
}