package net.ouska.gui;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.swing.JFrame;
@Documented
@Retention(RetentionPolicy.RUNTIME) 
public @interface Window {
	/**
	 * Title of the window. 
	 */
	String title() default "Dialog";
	/**
	 * X position of the window. 
	 */
	int x() default -1;
	/**
	 * Y position of the window.
	 */
    int y() default -1;
    /**
     * Forced width of the window.  
     */
    int width() default -1;
    /**
     * Forced height of the window.  
     */
    int height() default -1;
    /**
     * Vertical padding between boxes. 
     */
    int rowPadding() default 5;
    /**
     * Horizontal padding between boxes. 
     */
    int colPadding() default 5;
    /**
     * Vertical count of boxes. 
     */
    int rows() default 10;
    /**
     * Horizontal count of boxes. 
     */
    int cols() default 5;
    /**
     * Horizontal box width. 
     */
    int colWidth() default 50;
    /**
     * Vertical box height. 
     */
    int rowHeight() default 25;
    
    CloseOperation defaultCloseOperation() default CloseOperation.EXIT_ON_CLOSE;
}